package com.kevinmateolim.extralife;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;

public class SmsReceiver extends BroadcastReceiver {

    private static final int NOTIF_ID = 99;
    private static AssetFileDescriptor s1upSoundDescriptor = null;
    private static AssetFileDescriptor sCoinSoundDescriptor = null;


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

            String keyCoins = context.getString(R.string.key_coins);
            int numCoins = prefs.getInt(keyCoins, 0) + 1;
            Log.i("SmsReceiver", "numCoins:" + numCoins);
            prefs.edit().putInt(keyCoins, numCoins).commit();

            if (sCoinSoundDescriptor == null) {
                try {
                    sCoinSoundDescriptor = context.getAssets().openFd("smb_coin.wav");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            try {
                MediaPlayer mp = new MediaPlayer();

                mp.setDataSource(sCoinSoundDescriptor.getFileDescriptor(), sCoinSoundDescriptor.getStartOffset(), sCoinSoundDescriptor.getLength());

                if (numCoins % 100 == 0) {
                    do1Up(context, numCoins, prefs, mp);
                }

                mp.prepare();
                mp.start();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private void do1Up(Context context, int numCoins, SharedPreferences prefs, MediaPlayer mp) {
        String keyLives = context.getString(R.string.key_lives);
        int numLives = numCoins / 100;
        prefs.edit().putInt(keyLives, numLives).commit();

        if (s1upSoundDescriptor == null) {
            try {
                s1upSoundDescriptor = context.getAssets().openFd("smb_oneup.wav");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                try {
                    mediaPlayer.setOnCompletionListener(null);
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(s1upSoundDescriptor.getFileDescriptor(), s1upSoundDescriptor.getStartOffset(), s1upSoundDescriptor.getLength());
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle("You've earned an extra life!");
        mBuilder.setContentText("Total lives earned: " + numLives);
        mBuilder.setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, SettingsActivity.class), PendingIntent.FLAG_UPDATE_CURRENT));

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIF_ID, mBuilder.build());
    }
}